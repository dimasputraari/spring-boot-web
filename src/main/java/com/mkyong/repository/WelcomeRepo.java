package com.mkyong.repository;

import com.mkyong.model.Person;
import com.mkyong.model.Welcome;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class WelcomeRepo {

    @Autowired
    EntityManager entityManager;

    //public Welcome getWelcome() {
    //    return getWelcome();
    //}

    public List<Person> getAllPerson() {
        return entityManager.createQuery("SELECT x FROM Person x").getResultList();
    }
}
