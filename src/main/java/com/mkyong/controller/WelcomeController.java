package com.mkyong.controller;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mkyong.model.ConfigurationExample;
import com.mkyong.model.Person;
import com.mkyong.service.WelcomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class WelcomeController{

	@Autowired
	private WelcomeService welcomeService;

	@Autowired
	private ConfigurationExample configurationExample;

	// inject via application.properties
	@Value("${welcome.message:test}")
	private String message = "Hello World";

	//@Value("${test.another.properties}")
	//private String anotherProperties = "test";

	@RequestMapping("/")
	public String welcome(Map<String, Object> model) {
		//model.put("message", welcomeService.getWelcome());
		model.put("person", welcomeService.getAllPerson());
		model.put("configuration1", configurationExample.getSomeParam());
		model.put("configuration2", configurationExample.getInnerClass().getInnerParam());
		//model.put("anotherProperties", this.anotherProperties);
		return "welcome";
	}

	@RequestMapping(value = "/testJson",method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> testJson(Map<String, Object> model, @RequestBody Map<String, Object> request) {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> responseResult = new HashMap<>();
		Person orang = mapper.convertValue(request.get("isi"),Person.class);
		responseResult.put("nama", orang.getNama());
		responseResult.put("alamat", orang.getAlamat());
		return new ResponseEntity<>(responseResult, HttpStatus.OK);
	}
}