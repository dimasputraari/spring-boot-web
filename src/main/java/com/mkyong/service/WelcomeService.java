package com.mkyong.service;

import com.mkyong.model.Person;
import com.mkyong.model.Welcome;
import com.mkyong.repository.WelcomeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WelcomeService {

    @Autowired
    private WelcomeRepo welcomeRepo;

    //public Welcome getWelcome() {
    //    return welcomeRepo.getWelcome();
    //}

    public List<Person> getAllPerson() {
        return welcomeRepo.getAllPerson();
    }
}
